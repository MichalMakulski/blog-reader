const {ObjectID} = require('mongodb');
const jwt = require('jsonwebtoken');

const {Blog} = require('./../../models/blog');
const {User} = require('./../../models/user');

const userOneId = new ObjectID();
const userTwoId = new ObjectID();
const users = [{
  _id: userOneId,
  email: 'michal@test.com',
  password: 'userOnePass',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userOneId, access: 'auth'}, process.env.JWT_SECRET).toString()
  }]
}, {
  _id: userTwoId,
  email: 'alex@test.com',
  password: 'userTwoPass',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userTwoId, access: 'auth'}, process.env.JWT_SECRET).toString()
  }]
}]

const blogs = [{
  _id: new ObjectID(),
  url: 'makulscy.com',
  _creator: userOneId
}, {
  _id: new ObjectID(),
  url: 'urlopnaetacie.pl',
  _creator: userTwoId
}];

const populateBlogs = (done) => {
  Blog.remove({}).then(() => {
    return Blog.insertMany(blogs);
  }).then(() => done());
}

const populateUsers = (done) => {
  User.remove({}).then(() => {
    const userOne = new User(users[0]).save();
    const userTwo = new User(users[1]).save();

    Promise.all([userOne, userTwo]).then(() => done());
  })
}

module.exports = {blogs, populateBlogs, users, populateUsers};
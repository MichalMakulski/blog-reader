const mongoose = require('mongoose');

const BlogSchema = new mongoose.Schema({
  url: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  name: {
    type: String
  },
  _creator: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  }
});

BlogSchema.pre('save', function(next) {
  const blog = this;
  const name = blog.url.replace('.', '-');

  blog.name = name;
  next();
});

const Blog = mongoose.model('Blog', BlogSchema);

module.exports = {Blog};
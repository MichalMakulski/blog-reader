const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('./../server');

const {Blog} = require('./../models/blog');
const {User} = require('./../models/user');

const {blogs, populateBlogs, users, populateUsers} = require('./seed/seed');

beforeEach(populateUsers);
beforeEach(populateBlogs);

describe('POST /blogs', () => {
  it('should create a new blog list item', (done) => {
    const url = 'test.com';

    request(app)
      .post('/blogs')
      .set('x-auth', users[0].tokens[0].token)
      .send({url})
      .expect(200)
      .expect((res) => {
        expect(res.body.url).toBe(url);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Blog.find({url}).then((blogs) => {
          expect(blogs.length).toBe(1);
          expect(blogs[0].url).toBe(url);
          done();
        }).catch((e) => done(e));
      });
  });

  it('should not create todo with invalid body data', (done) => {
    request(app)
      .post('/blogs')
      .set('x-auth', users[0].tokens[0].token)
      .send({})
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Blog.find().then((blogs) => {
          expect(blogs.length).toBe(2);
          done();
        }).catch((e) => done(e));
      });
  });
});

describe('GET /blogs', () => {
  it('should get all blogs items', (done) => {
    request(app)
      .get('/blogs')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.blogs.length).toBe(1);
      })
      .end(done);
  });
});

describe('DELETE /blogs/:id', (done) => {

  it('should remove a blog item', (done) => {
    var hexId = blogs[1]._id.toHexString();

    request(app)
      .delete(`/blogs/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.blog._id).toBe(hexId)
      })
      .end((err, res) => {
          if (err) {
           return done(err);
          }

          Blog
            .findById(hexId)
            .then((blog) => {
              expect(blog).toNotExist();
              done();
            })
            .catch((err) => done(err));
      });
  });

  it('should not remove a blog created by other user', (done) => {
    var hexId = blogs[0]._id.toHexString();

    request(app)
      .delete(`/blogs/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end((err, res) => {
          if (err) {
           return done(err);
          }

          Blog
            .findById(hexId)
            .then((blog) => {
              expect(blog).toExist();
              done();
            })
            .catch((err) => done(err));
      });
  });

  it('should return 404 if blog not found', (done) => {
    var hexId = new ObjectID().toHexString();

    request(app)
      .delete(`/blog/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    request(app)
      .delete('/blog/123abc')
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });

});

describe('PATCH /blogs/:id', (done) => {
  it('should update the blog', (done) => {
    const hexId = blogs[0]._id.toHexString();
    const newName = 'Awesome blog';

    request(app)
      .patch(`/blogs/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .send({name: newName})
      .expect(200)
      .expect((res) => {
        expect(res.body.blog.name).toBe(newName);
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        done();
      });
  });

  it('should not update the blog created by other user', (done) => {
    const hexId = blogs[0]._id.toHexString();
    const newName = 'Awesome blog';

    request(app)
      .patch(`/blogs/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .send({name: newName})
      .expect(404)
      .end(done);
  });

  it('should return 404 if blog not found', (done) => {
    var hexId = new ObjectID().toHexString();

    request(app)
      .patch(`/blog/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    request(app)
      .patch('/blogs/123abc')
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });
});

describe('GET /users/me', () => {
  it('should return user if authenticated', (done) => {
    request(app)
      .get('/users/me')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).toBe(users[0]._id.toHexString());
        expect(res.body.email).toBe(users[0].email);
      })
      .end(done);
  });

  it('should return 401 if not authenticated', (done) => {
    request(app)
      .get('/users/me')
      .expect(401)
      .expect((res) => {
        expect(res.body).toEqual({});
      })
      .end(done);
  });
});

describe('POST /users', () => {
  it('should create a user', (done) => {
    var email = 'test@test.com';
    var password = '123mnb!';

    request(app)
      .post('/users')
      .send({email, password})
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toExist();
        expect(res.body.email).toBe(email);
      })
      .end((err) => {
        if (err) {
          return done(err);
        }

        User.findOne({email}).then((user) => {
          expect(user).toExist();
          expect(user.password).toNotBe(password);
          done();
        });
      });

  });

  it('should throw validation error', (done) => {
    const invalidUser = {
      email: 'test',
      password: 'abc123$'
    };

    request(app)
      .post('/users')
      .send(invalidUser)
      .expect(400)
      .expect((res) => {
        expect(res.body._message).toBe('User validation failed');
      })
      .end(done);
  });

  it('should not create a user if email in use', (done) => {
    const existingUser = {
      email: users[1].email,
      password: 'abc123$'
    };

    request(app)
      .post('/users')
      .send(existingUser)
      .expect(400)
      .end(done);
  });
});

describe('POST /users/login', () => {
  it('should login user and return auth token', (done) => {
    const {email, password} = users[1];

    request(app)
      .post('/users/login')
      .send({email, password})
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toExist();
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        User.findById(users[1]._id).then((user) => {
          expect(user.tokens[1]).toInclude({
            access: 'auth',
            token: res.headers['x-auth']
          });

          done();
        }).catch((err) => done(err));
      })
  });

  it('should return invalid login', (done) => {
    request(app)
      .post('/users/login')
      .send({
        email: users[1].email,
        password: 'abc123$'
      })
      .expect(400)
      .expect((res) => {
        expect(res.headers['x-auth']).toNotExist();
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        User.findById(users[1]._id).then((user) => {
          expect(user.tokens.length).toBe(1);

          done();
        }).catch((err) => done(err));
      })
  });
});

describe('DELETE /users/me/token', () => {
  it('should remove auth token', (done) => {
    request(app)
      .delete('/users/me/token')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        User.findById(users[0]._id).then((user) => {
          expect(user.tokens.length).toBe(0);
          done();
        }).catch((err) => done(err));
      })
  });
})
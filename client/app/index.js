import Vue from 'vue';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

import LoginForm from './components/login-form.vue'
import Dashboard from './components/dashboard.vue'

Vue.config.devtools = true;

const provideWrapper = (app, fn) => fn.bind(app);

const app = new Vue({
    el: '#app',
    components: {
      Dashboard,
      LoginForm
    },
    methods: {
      onLogin(token) {
        window.sessionStorage.setItem('token', token);
        window.sessionStorage.setItem('loggedin', 'true'),
        this.isLoggedIn = true;
      },
      onLogout() {
        window.sessionStorage.removeItem('token');
        window.sessionStorage.removeItem('loggedin'),
        this.isLoggedIn = false;
        this.error = null;
        this.loading = false;
      }
    },
    data() {
      return {
        isLoggedIn: window.sessionStorage.getItem('loggedin'),
        token: window.sessionStorage.getItem('token'),
        error: null,
        loading: false
      }
    },
    provide() {
      return {
        request: provideWrapper(this, function(url, opts, cb) {
          this.error = null;
          this.loading = true;

          return window.fetch(url, opts)
            .then((res) => res.json())
            .then(cb.bind(this))
            .then(() => {
              this.loading = false;
            });
        }),
        onError: provideWrapper(this, function(err) {
          this.err = err;
        })
      }
    }
});
require('./config/config');

const path = require('path');

var express = require('express');
var bodyParser = require('body-parser');
var {ObjectID} = require('mongodb');

const _ = require('lodash');

var {mongoose} = require('./db/mongoose');
var {Blog} = require('./models/blog');
var {User} = require('./models/user');

const {authenticate} = require('./middleware/authentication');

var app = express();
const port = process.env.PORT;

app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, '../client/build')));

app.post('/blogs', authenticate, (req, res) => {
  const blog = new Blog({
    url: req.body.url,
    _creator: req.user._id
  });

  blog.save().then((doc) => {
    res.send(doc);
  }, (e) => {
    res.status(400).send(e);
  });
});

app.get('/blogs', authenticate, (req, res) => {
  Blog.find({
    _creator: req.user._id
  }).then((blogs) => {
    res.send({blogs});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.delete('/blogs/:id', authenticate, (req, res) => {
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Blog.findOneAndRemove({
    _id: id,
    _creator: req.user._id
  }).then((blog) => {
    if (!blog) {
      return res.status(404).send();
    }

    res.send({blog});
  }).catch((e) => {
    res.status(400).send();
  });
});

app.patch('/blogs/:id', authenticate, (req, res) => {
  const body = _.pick(req.body, ['name']);
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Blog.findOneAndUpdate({
    _id: id,
    _creator: req.user._id
  }, {$set: body}, {new: true}).then((blog) => {
    if (!blog) {
      return res.status(404).send();
    }

    res.send({blog})
  }).catch((err) => {
    res.status(404).send();
  })
});

app.post('/users', (req, res) => {
  var user = new User(_.pick(req.body, ['email', 'password']));
  console.log(req.body)
  user.save()
    .then(() => user.generateAuthToken())
    .then((token) => {
      res.header('x-auth', token).send(user);
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});

app.get('/users/me', authenticate, (req, res) => {
  res.send(req.user);
});

app.post('/users/login', (req, res) => {
  var {email, password} = _.pick(req.body, ['email', 'password']);

  User.findByCredentials(email, password).then((user) => {
    return user.generateAuthToken().then((token) => {
      res.header('x-auth', token).send(user);
    })
  }).catch(() => {
    res.status(400).send({
      error: true,
      message: 'Something went wrong - check credentials'
    });
  });
});

app.delete('/users/me/token', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send();
  }).catch((err) => {
    res.status(400).send();
  })
});

app.listen(port, () => {
  console.log(`Started up at port ${port}`);
});

module.exports = {app};